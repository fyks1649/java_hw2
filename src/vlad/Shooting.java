package vlad;

import java.util.Scanner;

public class Shooting {
    public static void main(String[] args) {
        int SIZE = 6;
        System.out.println("All set. Get ready to rumble!" + "\n");
        String[][] shootingArea = new String[SIZE][SIZE];
        int randomNum1 = (int) (Math.random() * (SIZE - 1) + 1);
        int randomNum2 = (int) (Math.random() * (SIZE - 1) + 1);
        int coordinateX = 0;
        int coordinateY = 0;
        boolean work = true;

        while (randomNum1 != coordinateX && randomNum2 != coordinateY) {
            int count = 0;
            for (int i = 0; i < shootingArea.length; i++) {
                count = i;
                shootingArea[i][0] = String.valueOf(count);
                for (int j = 0; j < shootingArea.length; j++) {
                    count = j;
                    shootingArea[0][j] = String.valueOf(count);
                }
            }

            for (int i = 1; i < SIZE; i++) {
                for (int j = 1; j < SIZE; j++) {
                    shootingArea[i][j] = "-";
                }
            }

            shootingArea[randomNum1][randomNum2] = "f";

            showArray(shootingArea, SIZE);

            while (work) {
                Scanner scanner = new Scanner(System.in);
                System.out.print("Enter your row coordinate: ");
                coordinateX = scanner.nextInt();
                while (coordinateX < 1 || coordinateX > 5) {
                    System.out.print("Enter your row coordinate from 1 to 5: ");
                    coordinateX = scanner.nextInt();
                }
                System.out.print("Enter your column coordinate: ");
                coordinateY = scanner.nextInt();
                while (coordinateY < 1 || coordinateY > 5) {
                    System.out.print("Enter your column coordinate from 1 to 5: ");
                    coordinateY = scanner.nextInt();
                }
                shootingArea[coordinateX][coordinateY] = "*";
                if (coordinateX == randomNum1 && coordinateY == randomNum2) {
                    shootingArea[coordinateX][coordinateY] = "x";
                    work = false;
                }

                showArray(shootingArea, SIZE);
            }
            System.out.println("You have won!");
        }
    }

    private static void showArray(String[][] array, int SIZE) {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                System.out.print(array[i][j] + " | ");
            }
            System.out.println();
        }
    }
}
